package com.spring.springdto.DTO;

import java.util.ArrayList;
import java.util.List;

public class StudentPositionDTO {

    private Long id ;

    private String name ;

    private List<CourseResponse> courseResponseList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CourseResponse> getCourseResponseList() {
        return courseResponseList;
    }

    public void setCourseResponseList(List<CourseResponse> courseResponseList) {
        this.courseResponseList = courseResponseList;
    }
}
