package com.spring.springdto.DTO;

public class CourseDTO {

    private Long id ;

    private String name ;

    private String cost ;

    public CourseDTO() {
    }

    public CourseDTO(Long id, String name, String cost) {
        this.id = id;
        this.name = name;
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
