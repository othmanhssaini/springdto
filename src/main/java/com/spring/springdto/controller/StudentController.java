package com.spring.springdto.controller;

import com.spring.springdto.DTO.StudentDTO;

import com.spring.springdto.DTO.StudentPositionDTO;
import com.spring.springdto.DTO.StudentResponse;
import com.spring.springdto.model.Student;
import com.spring.springdto.service.StudentSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentSerice studentSerice ;

   @GetMapping("/allStudent")
    public List<StudentDTO> getAllStudent(){
        return studentSerice.getStudents();
    }

    @GetMapping("/allStudentR")
    public List<Student> getAllStudentAR(){
       return studentSerice.getStudentsAftereRelation();
    }

    //http://localhost:8081/student/getStudent/?student_id=1
    @GetMapping("/getStudent")
    public StudentResponse getStudent(@RequestParam("student_id") Long id ){
       return studentSerice.getStudent(id) ;
    }

    @GetMapping("/course-student")
    public StudentPositionDTO getStudentCourses(@RequestParam("courses_id_list") List<Long> ids){
       return studentSerice.getStudentCourses(ids);
    }
}
