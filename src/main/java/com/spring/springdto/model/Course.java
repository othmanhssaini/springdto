package com.spring.springdto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Course {

    @Id
    private Long id ;

    private String name ;

    private String cost ;

    private String time ;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    private Student student ;

    public Course() {
    }

    public Course(String name, String cost, String time) {
        this.name = name;
        this.cost = cost;
        this.time = time;
    }

    public Course(Long id, String name, String cost, String time) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost='" + cost + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
