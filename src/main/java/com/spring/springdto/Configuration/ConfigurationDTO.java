package com.spring.springdto.Configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationDTO {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
