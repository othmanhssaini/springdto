package com.spring.springdto.service;

import com.spring.springdto.DTO.CourseResponse;
import com.spring.springdto.DTO.StudentDTO;


import com.spring.springdto.DTO.StudentPositionDTO;
import com.spring.springdto.DTO.StudentResponse;
import com.spring.springdto.model.Course;
import com.spring.springdto.model.Student;
import com.spring.springdto.repository.CourseRepository;
import com.spring.springdto.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentSerice {

    @Autowired
    private StudentRepository studentRepository ;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CourseRepository courseRepository ;


    public List<StudentDTO> getStudents(){
        List<Student> students = studentRepository.findAll();
       List<StudentDTO>  studentDTOS = new ArrayList<>();
       for(int i=0 ; i<students.size();i++){
           StudentDTO studentDTO = new StudentDTO();
           studentDTO = modelMapper.map(students.get(i),StudentDTO.class);
           studentDTOS.add(studentDTO);
       }
       return studentDTOS ;
    }

    public List<Student> getStudentsAftereRelation(){
       return studentRepository.findAll();
    }

    public StudentResponse getStudent(Long id ){
      Student student = studentRepository.findById(id).get();
        StudentResponse studentResponse = new StudentResponse();
        studentResponse.setPhone("0634120919");
        modelMapper.map(student,studentResponse);
        return studentResponse;
    }

    public StudentPositionDTO getStudentCourses(List<Long> ids) {
        // recuperer student(id and name) :
        StudentPositionDTO studentPositionDTO = modelMapper.
                map(courseRepository.findStudentByCourseId(ids.get(0)),StudentPositionDTO.class);

        /*
        Student student = courseRepository.findStudentByCourseId(ids.get(0)) ;
        studentPositionDTO.setId(student.getId());
        studentPositionDTO.setName(student.getName()); */
        // recuprer la liste des cours :
        List<Course> courses = courseRepository.listCoursesInIds(ids);
        for(int i=0 ; i<courses.size() ; i++){
            CourseResponse courseResponse = modelMapper.map(courses.get(i),CourseResponse.class);
           /* courseResponse.setId(courses.get(i).getId());
            courseResponse.setCost(courses.get(i).getCost());*/
            studentPositionDTO.getCourseResponseList().add(courseResponse);
        }
        return studentPositionDTO ;
    }
}
