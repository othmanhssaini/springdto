package com.spring.springdto.service;

import com.spring.springdto.DTO.CourseDTO;
import com.spring.springdto.model.Course;
import com.spring.springdto.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository ;

    public List<CourseDTO> getCourses(){
    List<Course> courses = courseRepository.findAll();
    List<CourseDTO> courseDTOS = new ArrayList<>();
    for(int i=0 ; i<courses.size(); i++){
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(courses.get(i).getId());
        courseDTO.setName(courses.get(i).getName());
        courseDTO.setCost(courses.get(i).getCost());
        courseDTOS.add(courseDTO);
    }
        return courseDTOS ;
    }

    public List<Course> getAllCoursesAfterRelation(){
        return courseRepository.findAll();
    }
}
